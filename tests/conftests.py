import socket


def deny(*args, **kwargs):
    raise Exception('Network access disabled during testing.')


socket.socket = deny
