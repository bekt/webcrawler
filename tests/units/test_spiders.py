import pytest
import httpretty

from crawler.spiders import LocalGenericSpider
from scrapy.http import Request, TextResponse


@pytest.yield_fixture(scope='function')
def simple_html():
    with open('tests/data/simple.html', 'r') as f:
        yield f.read()


@pytest.yield_fixture(scope='function')
def hn_html():
    with open('tests/data/hn.html', 'r') as f:
        yield f.read()


@pytest.mark.usefixtures('simple_html', 'hn_html')
@httpretty.activate
class TestGenericSpider(object):

    def test_simple(self, simple_html):
        assert '<title>' in simple_html

    def test_lgs_base(self):
        url = 'https://example.org/a/b/c/'
        sp = LocalGenericSpider(url)
        assert sp.name == 'generic-local'
        assert sp.start_urls == [url]
        assert sp.allowed_domains == ['example.org']

    def test_lgs_invalid_uri(self):
        urls = ['foo, httpbin.org', 'foo bar']
        for url in urls:
            with pytest.raises(ValueError):
                LocalGenericSpider(url)

    def test_lgs_non_html(self):
        url = 'http://example.org/a/b/c'
        resp = TextResponse(url, body=b'content',
                            headers={'Content-Type': 'application/json'})
        sp = LocalGenericSpider(url)
        yields = list(sp.parse(resp))
        assert len(yields) == 0

    def test_lgs_single_html(self, simple_html):
        url = 'http://example.org/a/b/c'
        resp = TextResponse(url, body=simple_html.encode(),
                            headers={'Content-Type': 'text/html'})
        sp = LocalGenericSpider(url)
        yields = list(sp.parse(resp))
        assert len(yields) == 1

    def test_lgs_out_links(self):
        body = b"""<html><body>
          <a href="a.html">rel</a>
          <a href="/b/c/d">abs</a>
          <a href="http://example.org/bar">full</a>
          <a href="http://sub.example.org/zoo">nofollow</a>
          <a href="http://external.com">nofollow</a>
        </body></html>"""

        url = 'http://example.org/foo'
        resp = TextResponse(url, body=body,
                            headers={'Content-Type': 'text/html'})
        sp = LocalGenericSpider(url)
        out_links = sp.get_out_links(resp)
        assert sorted(out_links) == sorted(['/a.html', '/b/c/d', '/bar'])

    def test_lgs_build_item(self, hn_html):
        url = 'http://example.org/hn'
        resp = TextResponse(url, body=hn_html.encode(),
                            headers={'Content-Type': 'text/html'})
        sp = LocalGenericSpider(url)
        item = sp.build_item(resp)

        assert item['url'] == url
        assert item['title'] == 'Hacker News'
        assert len(item['source_hash']) == 32
        assert item['stylesheets'][0].startswith('news.css')
        assert item['scripts'][0].startswith('hn.js')
        assert 'y18.gif' in item['images']
        sample_links = {
            '/news', '/show', '/jobs', '/login?goto=news',
            '/item?id=15326535', '/vote?id=15326535&how=up&goto=news',
            '/hide?id=15326335&goto=news', '/bookmarklet.html', '/news?p=2'
        }
        assert sample_links.issubset(set(item['links']))

    def test_lgs_follows_urls(self):
        body = b"""<html><body>
          <a href="a.html">rel</a>
          <a href="/b/c/d">abs</a>
          <a href="http://example.org/bar">full</a>
          <a href="http://sub.example.org/zoo">nofollow</a>
          <a href="http://external.com">nofollow</a>
        </body></html>"""

        url = 'http://example.org/foo'
        resp = TextResponse(url, body=body,
                            headers={'Content-Type': 'text/html'})
        sp = LocalGenericSpider(url)
        yields = list(sp.parse(resp))

        assert len(yields) == 4
        assert type(yields[0]) == dict

        h = 'http://example.org'
        links = {h + '/a.html', h + '/b/c/d', h + '/bar'}
        for req in yields[1:]:
            assert type(req) == Request
            assert req.url in links
