## Simple Crawler

This is a basic website crawler built as a fun weekend project.

**[ 
  [DEMO](http://recordit.co/0L8iEEGmOv)
  | [Current Arch. Diagram](./arch.png)
  | [Improved Arch. Diagram](./arch-v2.png)
]**

## Noteable Highlights

* Very minimal and generic web page crawler that utilizes [scrapy](https://github.com/scrapy/scrapy).
* Python module -- can be used by any Python program (CLI tool, web service, etc).
* The crawler can run in a singple process or can be distributed among multiple workers using redis as the queue.
* Option to or not to respect robots.txt.
* Only crawls `Content-Type=text/html` pages (e.g no /rss).
* Only follows links to own domain.
* Outputs only the path of the link (instead of the full scheme+hostname+path).
The program can handle both relative and absolute URLs, as well as other protocols (e.g file, ftp, mail).
* Detects duplicate / seen URLs with request fingerprints.
* Many options can be tweaked via scrapy settings.
* Output is a jsonlines file. Each entry has:
    * `url`: full page url
    * `created_at`: scrape date in UTC
    * `title`: page title (`<title>`)
    * `source_gzip`: compressed HTML source. This is useful if we need to store the non-searchable source in a persistent database somewhere to save space.
    * `source_hash`: md5 hash of the HTML source. This is useful for detecting exact duplicate pages.
    * `links`: list of non-external (same domain) outgoing URLs from the page.
    * `images`: list of `img.src`'s in the page.
    * `stylesheets`: list of `link.rel=stylesheet`'s in the page.
    * `scripts`: list of `script.src`'s in the page.
    * `_spider_id`: ID of the spider that processed this page. This is mainly to demonstrate multiple worker consumers are working.

## Running

Requirements

* docker-compose 1.13+

There are two ways of running the program:

  1. Simple: single process
  2. Distributed workers

From code perspective, the logic is almost identical. See `crawler/spiders.py`.

### 1. Simple: single process

This method runs the program in a single process and does not have any dependencies (on redis for example).
Scrapy uses the Twisted networking library under the hood, so all the HTTP requests are asynchronous.

```
$ docker-compose build cli
$ docker-compose run --rm cli python crawler/cli.py <url>
```

You will see some useful information that mostly comes from scrapy. Once the job is finished, view the output:

```
$ jq -C . output/out-cli.jl | less -r
```

### 2. Distributed workers

This method is slightly complicated. It uses redis for as a queue, and has task workers consuming (polling) data from it.

```
$ docker-compose build

# Launch the services, with N consumers.
$ docker-compose up --scale worker=3
```

Once the containers are up, we can start inserting jobs into the queue.

```
# Make sure redis is ready.
$ docker-compose exec redis redis-cli ping

# Insert the root url to start crawling.
$ docker-compose exec redis redis-cli lpush generic-worker:root_url <url>
```

I'm using redis-cli directly in this example but you would want to write a wrapper client around it.

Once the crawlier is going, you can tail the `output/out-worker.jl` output file. After all the workers are finished, be sure to `docker-compose stop` so that output buffer is fully flushed.

```
$ jq -C . output/out-worker.jl | less -r
```

## Local Development

Although it is possible to develop locally with only docker/docker-compose installed, I would suggest you install python3 and [virtualenv](https://virtualenv.pypa.io/) for faster development.

```
$ virtualenv -p python3 venv
$ . venv/bin/activate.fish
$ pip install -r requirements-dev.txt

$ python crawler/cli.py <url>

# Run tests.
$ pytest tests/

# Run linter.
$ flake8 .
```

## Improvement Plans
* Improve input validation
* Improve error handling
  * Network issues
  * Dependency failures (redis, etc)
  * Unexpected exceptions
* Introduce statefulness (e.g resume a previously halted crawl)
* Flexible storage options
* Introduce crawl depth and/or limits
