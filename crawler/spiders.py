import hashlib
import logging
import os
from datetime import datetime
from urllib.parse import urlsplit

import scrapy
from scrapy_redis.spiders import RedisSpider


default_settings = {
    'USER_AGENT': 'Bekt-crawler/1.0',
    'ROBOTSTXT_OBEY': False
}


class BaseSpider(object):
    custom_settings = {**default_settings}

    def __init__(self):
        self.allowed_domains = None

    def parse(self, response):
        if not self.allowed_domains:
            self.allowed_domains = [parse_host(response.url)]
        if 'text/html' not in str(response.headers.get('Content-Type', '')):
            logging.info('Skipping non-HTML page: {}'.format(response.url))
            return
        yield self.build_item(response)
        for link in self.get_out_links(response):
            yield response.follow(link, callback=self.parse)

    def build_item(self, response):
        """Build item model for this specific page."""
        return {
            'url': response.url,
            'created_at': datetime.utcnow(),
            'title': response.css('title::text').extract_first(),
            'source_gzip': None,
            'source_hash': hashlib.md5(
                response.body.decode('utf-8').encode()).hexdigest(),
            'links': self.get_out_links(response),
            'images': list(set(response.css('img::attr(src)').extract())),
            'stylesheets': response.css(
                'link[rel=stylesheet]::attr(href)').extract(),
            'scripts': response.css('script::attr(src)').extract(),
            '_spider_name': self.name,
            '_spider_id': id(self),
        }

    def get_out_links(self, response, full_url=False):
        """Return internal links found in the page."""
        links = set()
        for link in response.css('a::attr(href)').extract():
            url = response.urljoin(link)
            if not self.should_follow(url):
                continue
            if not full_url:
                url = extract_path(url)
            links.add(url)
        return list(links)

    def should_follow(self, url, allow_sub=False):
        """Return True if the url is allowed to follow."""
        host = parse_host(url)
        return any((host == d) for d in self.allowed_domains or [])


class LocalGenericSpider(BaseSpider, scrapy.Spider):
    name = 'generic-local'

    def __init__(self, root_url=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not is_valid_url(root_url):
            raise ValueError('Invalid url: {}'.format(root_url))
        self.start_urls = [root_url]
        self.allowed_domains = [parse_host(root_url)]

class WorkerGenericSpider(BaseSpider, RedisSpider):
    name = 'generic-worker'
    redis_key = 'generic-worker:root_url'
    custom_settings = {**default_settings, **{
        'SCHEDULER': 'scrapy_redis.scheduler.Scheduler',
        'DUPEFILTER_CLASS': 'scrapy_redis.dupefilter.RFPDupeFilter',
        'REDIS_HOST': os.environ.get('REDIS_HOST', 'localhost')
    }}


def is_valid_url(url):
    pr = urlsplit(url)
    return pr.netloc and pr.path

def parse_host(url):
    return urlsplit(url).netloc.lower()

def extract_path(url):
    pr = urlsplit(url)
    path = pr.path
    if pr.query:
        path = '{}?{}'.format(path, pr.query)
    return path
