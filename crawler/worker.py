from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


if __name__ == '__main__':
    settings = get_project_settings()
    settings['FEED_FORMAT'] = 'jsonlines'
    settings['FEED_URI'] = '/data/out-worker.jl'
    crawler = CrawlerProcess(settings)
    crawler.crawl('generic-worker')
    crawler.start()
