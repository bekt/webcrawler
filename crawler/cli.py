import sys
import logging

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


if __name__ == '__main__':
    if len(sys.argv) > 1:
        url = sys.argv[1]
    else:
        url = 'https://news.ycombinator.com/'
        logging.info('Root URL not specified. Defaulting to {}'.format(url))

    settings = get_project_settings()
    settings['FEED_FORMAT'] = 'jsonlines'
    settings['FEED_URI'] = '/data/out-cli.jl'
    crawler = CrawlerProcess(settings)
    crawler.crawl('generic-local', root_url=url)
    crawler.start()
