# Global Scrapy settings.
#  http://doc.scrapy.org/en/latest/topics/settings.html
import os

SPIDER_MODULES = ['crawler.spiders']

CONCURRENT_REQUESTS = 32
LOG_LEVEL = 'DEBUG' if os.environ.get('ENV', '') == 'DEV' else 'INFO'
